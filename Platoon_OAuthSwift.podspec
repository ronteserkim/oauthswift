Pod::Spec.new do |s|
  s.name = 'Platoon_OAuthSwift'
  s.version = '1.2.5'
  s.license = 'MIT'
  s.summary = 'Swift based OAuth library for iOS and macOS.'
  s.homepage = 'https://bitbucket.org/ronteserkim/oauthswift'
  s.social_media_url = 'http://twitter.com/dongrify'
  s.authors = {
    'Dongri Jin' => 'dongrify@gmail.com'
  }
  s.source = { git: 'https://bitbucket.org/ronteserkim/oauthswift.git', tag: s.version }
  s.source_files = 'Sources/*.swift'
  s.swift_version = '4.1'
  s.ios.deployment_target = '9.0'
  s.requires_arc = false
end
